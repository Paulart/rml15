# Adoption citoyenne


## Choix individuel

L'adoption d'une monnaie libre, telle que pensée par la communauté de Duniter est un choix individuel.<br/>
*Note: Les défenseurs de l'allocation universelle en propose une [autre approche](https://allocation-universelle.net/theorie-relative-monnaie/).*

Chacun est libre de rejoindre une monnaie libre existante s'il le souhaite,
et de ce fait rejoint sa communauté d'utilisateurs.

Les plus aventureux peuvent aussi en créer de nouvelles et espérer fédérer une communauté autour de leur nouvelle monnaie
pour qu'elle prenne une valeur d'échange permettant de l'utiliser effectivement comme monnaie.

Par ce mécanisme d'adoption volontaire individuelle, chaque citoyen peut décider d'adopter
une monnaie libre dès maintenant (la June(Ǧ1) existe depuis le 8 mars 2017), sans attendre qu'un élu
se sente le pouvoir et le courage politique d'en mettre une en place.

### Comment est-ce possible ?
La Ǧ1, première monnaie libre, est créée via des outils informatiques en assurant le bon fonctionnement sans besoin
d'une autorité centrale pour contrôler que personne ne fraude. C'est ce qu'on appelle une crypto-monnaie.
Pour mieux comprendre, découvrez [les principes de vérification du bon fonctionnement par la communauté](#page_libre)
ainsi que [les détails du fonctionnement technique actuel](#page_technique).


## Durablement démocratique

Si des évolutions sont proposées pour le fonctionnement d'une telle monnaie,
elles devront recevoir le soutien de la majorité des avis exprimés¹, tel un référendum,
avant de pouvoir être mises en place.

Les mécanismes permettant de garantir cela sont développés
dans la page [choix techniques actuels](#page_technique).

¹ *Actuellement, dans la Ǧ1, le mécanisme qui permet d'exprimer son avis avec pouvoir décisionnaire
est de choisir la variante du programme Duniter qui reflète au mieux ses choix
(ou de développer et proposer à l'adoption sa propre variante si aucun choix ne convient), puis de s'y authentifier
et de laisser le programme tourner quotidiennement (plus nombreux, cela pourrait être une fois par semaine, mois,
voire par an) sur un ordinateur relié à internet afin qu'il "fasse entendre vos choix" au travers de l'algorithme
de consensus de la blockchain Ǧ1.*

*Il est certes plus simple de mettre un bulletin dans une urne mais contrairement aux élus, le système monétaire
de la Ǧ1 est contraint d'appliquer à la lettre le programme de la majorité.*
